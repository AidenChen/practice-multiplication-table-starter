package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start ,end)) {
            return null;
        }
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number >= 1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i <= end; i++) {
            sb.append(generateLine(start, i));
            if (i < end) {
                sb.append(String.format("%n"));
            }
        }
        return sb.toString();
    }

    public String generateLine(int start, int row) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i <= row; i++) {
            sb.append(generateSingleExpression(i, row));
            if (i < row) {
                sb.append("  ");
            }
        }
        return sb.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        StringBuilder sb = new StringBuilder();
        sb.append(multiplicand).append("*").append(multiplier).append("=").append(multiplicand * multiplier);
        return sb.toString();
    }
}
